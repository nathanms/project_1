bus = require('statebus/server')({
    port: 3004,
    client: function (client) {}
})

var fs = require('fs');
// file is included here:
eval(fs.readFileSync('./diff.js')+'');

function diff3_dig_in(b1, ancestor, b2) {
    var merger = Diff.diff3_merge(b1, ancestor, b2, false);
    var lines = [];
    for (var i = 0; i < merger.length; i++) {
        var item = merger[i];
        if (item.ok) {
            lines = lines.concat(item.ok);
        } else {
            var c = Diff.diff_comm(item.conflict.a, item.conflict.b);
            for (var j = 0; j < c.length; j++) {
                var inner = c[j];
                if (inner.common) {
                    lines = lines.concat(inner.common);
                } else {
                    // Incomeing file from client wins over head main in conflicts on server
                    lines = lines.concat(inner.file1);
                }
            }
        }
    }
    return(lines.join(""));
}

// TODO better way to deal with conflicts

bus('doc/*').to_save = function (obj) {


    //var waitTill = new Date(new Date().getTime() + 0.25 * 1000);
    //while(waitTill > new Date()){}


    // branch 2 is head of main branch (server branch)
    main_head = bus.fetch('/main_head_pointer')
    // can't do a merge without ancestor (base) and second branch from main (head)
    if (main_head.doc_key && obj.ancestor) {
        console.log('both main head and ancestor exist')
        // split is NOT unicode safe. don't use in japan
        b2 = bus.fetch(main_head.doc_key).text.split('')
        console.log('ancestor')
        console.log(obj.ancestor)
        ancestor = bus.fetch(obj.ancestor).text.split('')
        // for now we get ancestor in the same way, but update this when ancestor tag in place
        //ancestor = bus.fetch(main_head.doc_key).text
        // ancestor is the 'ancestor' on the main branch of the current users branch who is sending the file
    } else {
        b2 = '' 
        ancestor = '' 
    };

   // split is NOT unicode safe
   console.log(obj.text.split(''),ancestor,b2)
   merged = diff3_dig_in(obj.text.split(''),ancestor,b2)
   obj.text = merged 
   console.log(merged)

   // TODO generate patch and fix conflicts. maybe we only update patch somehow instead of sending whole thing
   // this is a patch of the doc of the user who just saved and result of three way merge
   // patch = Diff.diff_patch(b1,merged)

   // pointer to head of main (server) branch, this is pointer to branch 2 in 3-way-merge
   main_head = {key: '/main_head_pointer', doc_key: obj.key}

   // issue with concurrency here. classical issue with no compound transaction in nosql  
   // also if save.fire(main_head) works and save.fire(obj) fails, main head points to branch does not exist
   bus.save.fire(obj)
   bus.save.fire(main_head)
   //bus.save.abort(obj)   // To deny this save request!
}

// fetchs doc from main head (latest server copy). Then we merg on the client
bus('doc').to_fetch = function (key) {


    //var waitTill = new Date(new Date().getTime() + 0.1 * 1000);
    //while(waitTill > new Date()){}


    main_head = bus.fetch('/main_head_pointer')
    if (main_head.doc_key) {
        doc = bus.fetch(main_head.doc_key)
        // does not work, can't change keys in to_fetch
        //return {key: doc.key, text: doc.text}
        return {ancestor: doc.key, text: doc.text}
    } else {
        return {}
    };
}


/*



bus('doc').to_save = function (obj) {
   // Do some stuff...
   // ...and then fire the new state across the bus,
   // using one of these equivalent statements:
   merged = diff3_dig_in(obj.text,ancestor,b2)
   obj.text = merged 

   // TODO generate patch and fix conflicts. maybe we only update patch somehow instead of sending whole thing
   // this is a patch of the doc of the user who just saved and result of three way merge
   patch = Diff.diff_patch(b1,merged)

   //bus.save.abort(obj)   // To deny this save request!
   bus.save.fire(obj)
}


bus('one_plus/*').to_fetch = function (key) {
   // The *individual* key being fetched is passed to this function as "key"
   var num = Number(key.split('/')[1])
   return {result: 1 + num}
}

bus('two_plus/*').to_fetch = function (key) {
   // The *individual* key being fetched is passed to this function as "key"
   var num = Number(key.split('/')[1])
   return {result: 2 + num}
}

bus('hell3').to_fetch = function (key) {
   // Do some stuff...
   // ...and then fire the new state across the bus,
   // using one of these equivalent statements:
   //return {key: 'doc', text: 'working'}
   bus.save.fire({key: 'hell3', text: 'fucking work', kill: 'me'})  // Use this from within callbacks
}

bus('doc').to_fetch = function (key) {
   // Do some stuff...
   // ...and then fire the new state across the bus,
   // using one of these equivalent statements:
   return {key: 'doc', text: 'working?'}



function demo_diff3_dig_in(f1,f0,f2) {
    var merger = Diff.diff3_merge(f1, f0, f2, false);
    var lines = [];
    for (var i = 0; i < merger.length; i++) {
        var item = merger[i];
        if (item.ok) {
            lines = lines.concat(item.ok);
        } else {
            var c = Diff.diff_comm(item.conflict.a, item.conflict.b);
            for (var j = 0; j < c.length; j++) {
                var inner = c[j];
                if (inner.common) {
                    lines = lines.concat(inner.common);
                } else {
                    lines = lines.concat(["\n<<<<<<<<<\n"], inner.file1,
                                         ["\n=========\n"], inner.file2,
                                         ["\n>>>>>>>>>\n"]);
                }
            }
        }
    }
    return(lines.join(""));
}

*/

console.log('starting test')
var fs = require('fs');
// file is included here:
eval(fs.readFileSync('./diff.js')+'');

var crypto = require("crypto");

function diff3_dig_in(b1, ancestor, b2, control_branch) {
    // set control_file == 'file1' or 'file2'
    var merger = Diff.diff3_merge(b1, ancestor, b2, false);
    var lines = [];
    for (var i = 0; i < merger.length; i++) {
        var item = merger[i];
        if (item.ok) {
            lines = lines.concat(item.ok);
        } else {
            var c = Diff.diff_comm(item.conflict.a, item.conflict.b);
            for (var j = 0; j < c.length; j++) {
                var inner = c[j];
                if (inner.common) {
                    lines = lines.concat(inner.common);
                } else {
                    // Incomeing file from client wins over head main in conflicts on server
                    lines = lines.concat(inner[control_branch]);
                }
            }
        }
    }
    return(lines.join(""));
}

var Client = {

  //conn: {client: crypto.randomBytes(20).toString('hex') },
  conn: {client: (Math.random() + 1).toString(36).substring(7)},

  server: {},

  // current client side values. used in test now
  window: {
    current_text: '',
    ancestor_text: '',
    ancestor_key: '',
  },

  // incoming text from server. do not need now. but could use for future tests
  doc: {
    key: '',
    text: '',
    ancestor: '',
  },

  user_enter_text: function(text) {
    // save to server and update current window
    console.log('user entered text:', text)
    hash = this.conn.client + '@' + Date.now() + (Math.random() + 1).toString(36).substring(7)
    n_doc = {key: '/doc/' + hash}
    n_doc.text = text 
    n_doc.ancestor = this.doc.ancestor
    // in index.html save(n_doc)
    this.server.receive(n_doc)
    this.window.current_text = text
  },

  receive: function(doc) {
    console.log('client received sent text:', doc)
    // merge and save to server if necessary
    this.window.ancestor_current = doc.text
    merged = diff3_dig_in(doc.text.split(''),this.window.ancestor_text.split(''),this.window.current_text.split(''))
    if (merged !== doc.text) {
      hash = this.conn.client + '@' + Date.now() + (Math.random() + 1).toString(36).substring(7)
      n_doc = {key: '/doc/' + hash}
      n_doc.text = this.doc.text
      n_doc.ancestor = this.doc.ancestor
      // in index.html save(n_doc)
      this.server.receive(n_doc)
    } else {
      merged = doc.text
    };
    this.window.ancestor_key = doc.ancestor
    this.window.ancestor_text = doc.text
    this.window.current_text = merged
    return merged
  }

}

var Server = {

  get_current_text: function() {
    return this.docs[this.main_head_pointer].text
  },


  // dict of docs. after each update from the client a new one with a new key added 
  docs: {
    key:
    {
      key: '',
      text: '',
    }
  },

  connections:  [],
  // pointer to key of the latest version of doc on the server
  main_head_pointer: '',
  window_current_text: '',

  set_connections: function(clients) {
    for (var i=0; i < arguments.length; i++) {
      this.connections.push(arguments[i])
    }
  },

  fire : function(doc,conns) {
    conns.forEach(function(c){
      c.receive(doc)
    });
  },

  receive: function(obj) {
    console.log('server receive received', obj);
    // main_head_doc is latest version of server doc
    main_head_doc = this.docs[this.main_head_pointer]
    if (main_head_doc && obj.ancestor) {
      b2 = main_head_doc.text.split('')
      ancestor = docs[obj.ancestor].text.splt('')
    } else {
      b2 = ''
      ancestor = ''
    }
    merged = diff3_dig_in(obj.text.split(''),ancestor,b2)
    obj.text = merged

    // this obj is now the head of the server branch. add it to the docs database
    this.main_head_pointer = obj.key
    this.docs[obj.key] = {
        key: obj.key,
        text: obj.text,
    }

    // to mimic how db retrieved on server, we retrieve latest and sent to clients
    main_head_doc = this.docs[this.main_head_pointer]
    if (main_head_doc.key) {
      // kind of awkward, we need to do it like this till versioning is working 
      // this ancestor_key will be used for merges when text comes back from client 
      main_head_doc.ancestor = main_head_doc.key
      this.fire(main_head_doc,this.connections)
    } else {
      this.fire({},this.connections)
    };

  },

  update_clients: function() {
    // probably do not need this. could use to test an update client only with no new doc
    console.log('updated all clients with current doc from head');
  },
};


function example_test(text) {
// client_a creates new doc and sends to the server, server merges. client_b is then updated. test if doc the same on c_a,c_b, s

  // each test case below cycles through bunch of examples 
  var client_a = Object.create(Client)
  var client_b = Object.create(Client)
  var server = Object.create(Server) 

  // need to do this setup and connect at each start
  server.set_connections(client_a,client_b)
  client_a.server = server
  client_b.server = server
  client_a.user_enter_text(text)

  var r1 = client_a.window.current_text === client_b.window.current_text 
  var r2 = client_b.window.current_text === server.get_current_text()
  var r3 = client_a.window.current_text === text
  result = r1 && r2 && r3
  if (result) {
    console.log(arguments.callee.name, 'PASSED')
  } else {
    console.log(arguments.callee.name, 'FAILED')
  }
  return result
};


function basic_test(text) {
// client_a creates new doc and sends to the server, server merges. client_b is then updated.
// client_b edits new text. test if c_a,c_b,s same 
  client_a = Object.create(Client)
  client_b = Object.create(Client)
  server = Object.create(Server) 

  // need to do this setup and connect at each start
  server.set_connections(client_a,client_b)
  client_a.server = server
  client_b.server = server

  client_a.user_enter_text(text)
  // update the text and add it into client_b
  text = 'new $V test in the begin' + client_b.window.current_text + ' this is \nsome more text dsf #\n'
  client_b.user_enter_text(text)

  var r1 = client_a.window.current_text === client_b.window.current_text
  var r2 = client_b.window.current_text === server.get_current_text()
  var r3 = client_a.window.current_text === text
  result = r1 && r2 && r3
  if (result) {
    console.log(arguments.callee.name, 'PASSED')
  } else {
    console.log(arguments.callee.name, 'FAILED')
  }
  return result

};

// client_a creates new doc and sends to the server, server merges. client_b is then updated. cycle a few times. same doc?

function basic_client_cycle_test(text) {
  client_a = Object.create(Client)
  client_b = Object.create(Client)
  server = Object.create(Server) 
};

// there is already a doc on the server. client_a, client_b, and client_c connect. Each client makes a different alteration on the doc. same doc? 


// bunch of clients, bunch of docs, clients make different edits in beginning, middle and end of doc. cycle 


example_test('hi there')
basic_test('hi there')




